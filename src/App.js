import './App.css';
import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Switch, Route, NavLink} from "react-router-dom"
import ArticleDetails from './Componets/ArticleDetails';
import Articles from './Componets/Articles';

function App() {
 
  return <div className="App">
    <Route exact path="/" component={Articles}/>
    <Route exact path="/articles/:id" component={ArticleDetails}/>
    
    

  </div>
}

export default App;






