/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-no-target-blank */
import React, {useState, useEffect} from 'react';

const Articles = () => {
 
    const [articles,  setArticles] = useState (
      [])
  const [popular, setPopular] = useState(
      'Popular')
  
   const [isLoading, setIsLoading] = useState(true)   
  
   useEffect(() => {
       
      const getPopularArticles = async () => {
      try {
      
           const res = await fetch('https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=dOWHYNJHmSisO6tQw0kC9v8601E1HCxL')
           
           const articles = await res.json();
          // console.log(articles);
           setArticles(articles.results);
          // console.log(results);
       }   
  
       catch (error) {
         console.log(error); 
      }
  }
          getPopularArticles()
   }, )
  
    return (
      <>
      <div className="showcase">
      <div className= "overlay px-5">
      <h1 className="text-4*1 font-bold text-white text-center mb-4"> Welcome to the News App </h1>
     
      </div>
      </div>
      <section className="grid grid-cols-1 gap-10 px-5 pt-10 pb-20">
       {articles.map((article) =>{
      const {abstract, title, byline, section , published_date, id, url, asset_id} =article;
  
      return (
        <article key={id} className="bg-white py-10 px-5 rounded-lg lg:w-9/12 lg:mx-auto">
          
       <p> {abstract}</p>
      
  
       <ul className="my-4">
         <li><span className="font-bold">Author/s: </span> {byline}</li>
         <li> <span className="font-bold"> Section: </span>  {section}</li>
         <li> <span className="font-bold">Date Published: </span> {published_date}</li>
       </ul>
  
       <a href= {url} target = "_blank" className="underline">Article Link</a>  
       <a href= {'/articles/' + id} target = "_blank" className="underline px-10">Read More</a>

     
     
        </article>
      )
       })}
      </section>
  
      </>
    );
  }
  
  export default Articles;
  